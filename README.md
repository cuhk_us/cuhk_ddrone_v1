# README #

## Quick summary

This package is for the Ddrone Project. As there are many sub-packages and versions with different sensors such as UWB or VIO, the repo is built to clarify some setups.

## Set up 
### Download packages 
#### Method 1(recommand)
```
$ git clone https://bitbucket.org/nusuav/ddrone.git
$ git submodule init
$ git submodule update
```
or
```
$ git clone --recurse-submodules https://bitbucket.org/nusuav/ddrone.git
```
And then input your user name and password.
Clone with HTTPS and want to avoid entering username / password each time, you can store credentials into cache with below command:
```
$git config --global credential.helper 'cache --timeout 3600'
```
where 3600 (seconds) means 1 hour, you may change it as per your requirement.
#### Method 2
Several packages have to be downloaded from bitbucket, which are listed as follows. Don't forget to check out to the correct branch.

1. common packages
The common packages are used on all the drones, which include: 
    - mavros (branch: kinetic-Ddrone)
    - nus_msgs (branch: kinetic-Ddrone)
    - nus_task_manager (branch: kinetic-Ddrone-dev)
    - nus_reference_generator (branch: kinetic-Ddrone-dev)
    - nus_launch (branch: kinetic-Ddrone)

2. uwb packages.
Download this list if you are setting the drone with UWB.
    - nus_uwb (branch: test)
    - pkg_common (branch: kinetic-Ddrone) 
    - pub_uav_state (branch: kinetic-Ddrone)

3. Loosely coupled VIO (EKF)
Download this list if you are setting the drone with zed camera and imu from PX4.
    - nus_ekf_vio (branch: master)
    - pub_uav_state (branch: msckf-odom)

4.  Tightly coupled VIO (MSCKF)
Download this list if you are setting the drone with pointgrey camera and additional imu.
    - nus_tlsensor (branch: master)
    - pub_uav_state (branch: msckf-odom)

5. simulation
    - nus_unity_socket (branch: kinetic-Ddrone)

6. run git_version_control.sh to checkout to correct branches

### What do you need to check or config
All the param can be set in the v1_Ddrone_vio.launch file and through which pass the each specific files.
1. system id of your drone
Please change the system id in the following files:
- mavros/launch/px4.launch (system_id & tgt_system, both are the pixhawk's id)
- mavros/launch/px4_config.yaml (uav_sys_id_list)
- nus_reference_generator/launch/reference_generator.launch (self_system_id & target_system_id, both are the pixhawk's id)

If you are using uwb, please also check:
- nus_uwb/launch/xxx.launch (remap from="/mavros/imu/data/sys_id_1" to="/mavros/imu/data/sys_id_[your_id]")

2. Config weather to use the heading from VIO or magnetometer on GCS

3. Put your waypoint or trajectory file under nus_task_manager/config folder.
- Set the NavigateWithTraj param in v1_Ddrone_vio.launch to true to make the drone use trajectory mode, otherwise it will fly in waypoint mode. 
- set "waypoints_file" or "trajectory_file" to the file you would like to use.
- The waypoint file should follow the format as following:
```
# NWU frame
# px py pz 
waypoints:
- [ 0.0,  0.0, 0.0]
- [ 5.5,  9.7, 2.0]
- [ 20.5,  9.7, 2.0]
- [ 20.5,  9.7, 0.0]
```
Noted that pz=0.0 to pz=5.5 is to be recognized as a take-off trigger and pz=2.0 to pz=0.0 is to be regarded as a landing signal.
- The trajectory file should follow the format as following:
```
# NWU frame
# px py pz vx vy vz ax ay az yaw
waypoints:
- [  0.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00]
- [  0.00,   0.00,   2.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00,   0.00]
- [  0.00,  -0.00,   2.00,   0.00,  -0.00,   0.00,   0.00,  -0.00,   0.00,   0.00]
...
- [ 12.00,  -0.00,   2.00,   0.00,   0.00,  -0.00,  -0.00,  -0.00,   0.00,   0.00]
- [ 12.00,  -0.00,   0.00,   0.00,   0.00,  -0.00,  -0.00,  -0.00,   0.00,   0.00]
```
The take-off and landing signal are the same as the waypoints file.

4. If you are using the uwb, please check if you choose the correct config file, as the setup for different locations has various scales.

### Compile
```
$ catkin_make -DCMAKE_BUILD_TYPE=Release
```
### Run
Three methods to launch the drone as your requirement:
1. **Launch Ddrone with zed vio** (Todo: add one more script for uwb)
```
$ roslaunch nus_launch v1_Ddrone_vio.launch
```
- **Important params to check before launching**
    - system_id
    - tgt_system
    - group params for reference generator
    - NavigateWithTraj 
    - waypoints_file
    - trajectory_file
    - dist_chec
- **Important data to check before launching**
    - vio output /ekf_fusion/pose

2. Launch Ddrone with pointgrey
- Choose the correct driver of flea3 based on your platform
There are two driver under nus_tlsensor/driver/flea3/flycapture/x86_arm_bk. Replace the "include" and "lib" folders in nus_tlsensor/driver/flea3/flycapture with the correct version: arm for tx2 and x86 for NUC
- Run
```
$ roscd nus_launch/scripts
$ ./Ddrone_msckf.sh
$ roscd catkin_ws/src/nus_tlsensor/script
$ ./whichever_ you_choose.sh
```
3. Launch Ddrone in simulation mode
- Configure params
    - simulator_server_ip 
    - system_id & tgt_system 
- Run 
```
$roslaunch nus_launch Ddrone_sim.launch
```
- Send two /mavros/cmd/vehicle_cmd/sys_id_255_mavros/Cmd command through rqt
    - a. params1=7.0,command=1,target_system=1,target_component=50;
    - b. params1=14.0,command=1,target_system=1,target_component=50;
- Check if the screen output of nus_reference_generator is correct 

### FAQ
1. No data output from px4 after launch mavros
- Check if the system_is has been set correctly.
- Check if the "fcu_url" param in the px4.launch is correct.  You may need to set to /dev/ttyUSB0 instead of /dev/pixhawk.

2. No msckf odom output
Check if the pointgrey camera has been triggered by the IMU, which means check the serial port or give the port a+rw permission.

3. Can't see any data in GCS
- Check if the windows firewall is open.
- Check if you config the right ip under the set_parameters file on windows

### Tips

### To be continued
