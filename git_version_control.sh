cd ../mavros
git checkout kinetic-Ddrone
echo "branch of mavros: "
git branch

cd ../nus_msgs
git checkout kinetic-Ddrone
echo "branch of nus_msgs: "
git branch

cd ../nus_task_manager
git checkout kinetic-Ddrone-dev
echo "branch of nus_task_manager: "
git branch

cd ../nus_reference_generator
git checkout kinetic-Ddrone-dev
echo "branch of nus_reference_generator: "
git branch

cd ../nus_launch
git checkout kinetic-Ddrone
echo "branch of nus_launch: "
git branch

cd ../nus_uwb
git checkout test
echo "branch of nus_uwb: "
git branch

cd ../pkg_common
git checkout kinetic-Ddrone
echo "branch of pkg_common: "
git branch

cd ../pub_uav_state
git checkout kinetic-Ddrone
echo "branch of pub_uav_state: "
git branch

cd ../nus_unity_socket
git checkout kinetic-Ddrone
echo "branch of nus_unity_socket: "
git branch


